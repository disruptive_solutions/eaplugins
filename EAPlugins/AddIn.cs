﻿using EA;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace EAPlugins
{
    public class AddIn
    {
        // define menu constants
        const string menuHeader = "-&VanBoeijen";
        const string menuImport = "&Import files...";
        const string menuAbout = "&About...";

        public static Facade facade = null;

        // Called Before EA starts to check Add-In Exists
        public string EA_Connect(Repository repository)
        {
            // nothing special
            facade =  Facade.Instance;
            return "EaPlugins.AddIn - connected";
        }

        // Called when user Click Add-Ins Menu item.
        public object EA_GetMenuItems(Repository repository,
            string location, string menuName)
        {
            switch (menuName)
            {
                case "":
                    return menuHeader;
                // defines the submenu options
                case menuHeader:
                    string[] subMenus = { menuImport, menuAbout };
                    return subMenus;
            }
            return "";
        }

        // Sets the state of the menu depending if there is
        // an active project or not
        static bool IsProjectOpen(Repository repository)
        {
            try
            {
                //System.Windows.MessageBox.Show("Repository BF: "
                 //       + repository);
                Facade.setEARepository(repository);
                return null != repository.Models;
            }
            catch
            {
                return false;
            }
        }

        // Called once Menu has been opened to see what menu
        // items are active.
        public void EA_GetMenuState(Repository repository,
            string location, string menuName, string itemName,
            ref bool isEnabled, ref bool isChecked)
        {
            if (IsProjectOpen(repository))
            {
                switch (itemName)
                {
                    // define the state of the hello menu option
                    case menuHeader:
                        isEnabled = true;
                        break;
                    // define the state of the hello menu option
                    case menuImport:
                        isEnabled = true;
                        break;
                    // define the state of the hello menu option
                    case menuAbout:
                        isEnabled = true;
                        break;
                    // there shouldn't be any other, but just in case disable it.
                    default:
                        isEnabled = false;
                        break;
                }
                //if (itemName == "Project statistic...")
                //    isEnabled = true;
            }
            else
                // If no open project, disable all menu options
                isEnabled = false;
        }

        // Called when user makes a selection in the menu.
        // This is your main exit point to the rest of your Add-in
        public void EA_MenuClick(Repository repository,
            string location, string menuName, string itemName)
        {
            switch (itemName)
            {
                case menuImport:
                    var count = 0;
                    frmParent frmWizard = new frmParent();
                    frmWizard.Show();

                    /*
                    foreach (Package model in repository.Models)
                        foreach (Package package in model.Packages)
                            count += CountClasses(package);
                    System.Windows.MessageBox.Show("This project contains "
                        + count + " " + (count == 1 ? "class" : "classes"));*/
                    break;
                case menuAbout:
                    AboutBox1 a = new AboutBox1();
                    a.Show();
                    break;
            }
        }

        private static int CountClasses(Package package)
        {
            var count = 0;
            foreach (Element e in package.Elements)
                if (e.Type == "Class")
                    count++;
            foreach (Package p in package.Packages)
                count += CountClasses(p);
            return count;
        }

    }
}
