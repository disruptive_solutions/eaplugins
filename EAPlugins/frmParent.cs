﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EAPlugins
{
    public partial class frmParent : Form
    {
        Form[] frm = { new frm1(), new frm2() };
        int top = -1;
        int count=2;

        public frmParent()
        {
            count = frm.Count();
            InitializeComponent();
        }

        private void LoadForm()
        {
            frm[top].TopLevel = false;
            frm[top].AutoScroll = true;
            frm[top].Dock = DockStyle.Fill;
            this.panelCnt.Controls.Clear();
            this.panelCnt.Controls.Add(frm[top]);
            frm[top].Show();
        }

        private void frmParent_Load(object sender, EventArgs e)
        {
            Next();
        }

        private void Next()
        {

            top++;
            if (top >= count)
            {
                return;
            }
            else
            {
                btnNext.Enabled = true;
                LoadForm();
                if (top + 1 == count)
                {
                    btnNext.Enabled = false;
                }
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Next();
        }
    }

}
