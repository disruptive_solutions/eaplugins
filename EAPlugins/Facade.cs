﻿using System;
using System.Collections.Generic;
using System.Linq;
using EA;
using System.Text;
using System.Threading.Tasks;

namespace EAPlugins
{
    public class Facade
    {
        private static Facade instance;
        private static Repository repository = null;

        public static void setEARepository(Repository repository) {
            repository = repository;
        }

        private Facade()
        {
            
        }

        public static Repository getEARepository()
        {
            return repository;
        }

        public static Facade Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Facade();
                }
                return instance;
            }
        }
    }
}
