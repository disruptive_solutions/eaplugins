﻿using EA;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EAPlugins
{
    public partial class frm1 : Form
    {
        public frm1()
        {
            InitializeComponent();

            // start off by adding a base treeview node

            System.Windows.MessageBox.Show("Repository: "
                        + Facade.getEARepository());

            foreach (Package model in Facade.getEARepository().Models)
            {
                foreach (Package package in model.Packages)
                {
                    TreeNode mainNode = new TreeNode();
                    mainNode.Name = "mainNode";
                    mainNode.Text = "Main";
                    //frm1.Nodes.Add(mainNode);
                }
            }

        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

    }
}
